Improvements in the Application

1. We can add expiry_time for every url and after the expiration we will mark it in_active or we can also move the in_active URLS into another table.

2. We can also do database sharding.

3. We can implement LRU Cache for better caching.

4. We can do all the database operation in background.

5. We can also add UTM paramters for better analytics.

6. We can also work on the slug.