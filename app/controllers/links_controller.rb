class LinksController < ApplicationController
  before_action :set_link, only: [:show, :detail]
  before_action :authenticate_user!, except: [:show, :detail]



  # GET /links
  # GET /links.json
  def index
    @host_name = request.server_name
    @links = Link.order(:created_at).paginate(page: params[:page], per_page: 5)
  end

  def new
    @link = Link.new
    respond_to do |format|
      format.js
    end
  end

  def detail
    if @link
      render json: { status: :ok, data: @link }
    else
      render json: { status: :not_found }
    end
  end

  # GET /links/1
  # GET /links/1.json
  def show
    if @link
      redirect_to @link.given_url
    else
      redirect_to root_url, alert: 'No matching url found.'
    end
  end


  # POST /links
  # POST /links.json
  def create
    @link = UseCase::CreateLink.new(link_params).call

    respond_to do |format|
      if @link.save
        format.html { redirect_to links_path, notice: 'Link was successfully created.' }
        format.json { render :show, status: :created, location: @link }
      else
        format.html { render :new }
        format.json { render json: @link.errors, status: :unprocessable_entity }
      end
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.

    def set_link
      @link = UseCase::GetLink.new(slug: params[:slug]).call
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def link_params
      params.require(:link).permit(:given_url).to_h.merge!(user_id: current_user.id)
    end
  end
