require 'rails_helper'

RSpec.describe LinksController do

  describe "GET index" do
    let(:user) {User.create(email: 'test@mail.com', password: '12345678', password_confirmation: '12345678' )}
    before(:each) { sign_in user }
    after(:each)  { sign_out user}
    it "assigns @links" do
      link = Link.create(given_url: 'http://www.test.com/')
      get :index
      expect(assigns(:links)).to eq([link])
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end
  end

  
  describe "POST #create" do
    context "with valid attributes" do
      it "creates a new link" do
        expect{
          post :create, link: Link.create(given_url: 'http://www.google.com')
        }.to change(Link,:count).by(1)
      end
    end
  end
end
