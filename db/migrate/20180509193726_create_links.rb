class CreateLinks < ActiveRecord::Migration[5.2]
  def change
    create_table :links, id: :uuid do |t|
      t.string :given_url
      t.string :slug
      t.integer :use_count, default: 0

      t.timestamps
    end
    add_index :links, :slug, unique: true
  end
end
